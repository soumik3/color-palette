import './App.css';
import React , { useState } from 'react';
import { BlockPicker } from 'react-color';

function App() {
  const [color, setColor] = useState('#fff')
  //const [bgColor, setBgColor] = useState('#fff')
  return (
    <div style={{background: color}}>
      <span>
        {"Pick a color"}
        </span>
       <BlockPicker color={color} 
     onChange={updatedColor => setColor(updatedColor.hex)}
     />
    </div>
  );
}

export default App;
